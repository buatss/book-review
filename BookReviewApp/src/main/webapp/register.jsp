<%@ page import="com.bookReviewApp.controller.util.CommandEnum"%>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
    <link rel="stylesheet" href="css/core.css" />
    <title>Book Review App</title>
</head>

<body>
    <div class="register">
        <center>
            <h3>Sign up!</h3>
                <div class="error">
                    <p>
                        <%
                            Object message = request.getAttribute("message");
                            if(message==null) {
                            message = "";
                            }
                            out.println(message);
                        %>
                    </p>
                </div>
            <table>
                <form action="FrontController" method="post">
                    <tr>
                        <th>Nickname:</th>
                        <th><input type="text" name="nickname" /></th>
                    </tr>
                    <tr>
                        <th>Email:</th>
                        <th><input type="text" name="email" /></th>
                    </tr>
                    <th>Password:</th>
                    <th><input type="password" name="password" /></th>
                    </tr>
                    <th><input type="hidden" name="command" value=<%out.println(CommandEnum.REGISTER.getCommand());%>/></th>
                    <tr>
                        <th></th>
                        <th><input type="submit" value="register" /></th>
                    </tr>
                </form>
                <form action="FrontController" method="post">
                     <th></th>
                     <input type="hidden" name="command" value=<%out.println(CommandEnum.REDIRECT_TO_CONTENT.getCommand());%> />
                     <th><input type="submit" value="redirect To Content" /></th>
                 </form>
            </table>
        </center>
    </div>
</body>

</html>