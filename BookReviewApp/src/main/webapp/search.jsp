<%@ page import="com.bookReviewApp.controller.util.CommandEnum" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.util.ArrayList" %>
<%@page import="com.bookReviewApp.domain.Page" %>
<%@page import="com.bookReviewApp.domain.dto.BookDto" %>
<%@page import="com.bookReviewApp.controller.util.DefaultActions" %>
<%@page import="java.util.List" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
    <link rel="stylesheet" href="css/core.css" />
    <title>Book Review App</title>
</head>

<body>
    <div class="login">
        <center>
            <h3>Welcome to content page with searching results</h3>
            <form action="FrontController" method="post">
                <tr>
                    <th>Title</th>
                    <th><input type="text" name="search" /></th>
                <tr>
                    <th></th>
                    <th>
                        <input type="hidden" name="command"
                            value=<%out.println(CommandEnum.FIND_BOOK_BY_TITLE.getCommand());%>
                        />
                    <th><input type="submit" value="search" /></th>
                    </th>
                </tr>
            </form>
        </center>
    </div>

    <div class="defaultBooks">
        <!-- display by default several books -->
        <h3>Books</h3>
        <% List<BookDto> books = (List<BookDto>) request.getAttribute("books");
            Iterator<BookDto> iterator = books.iterator();
                while (iterator.hasNext()) {
                BookDto bookDto = iterator.next();
                %>
                    <div class="bookContainer">
                        <p>Title: <%=bookDto.getTitle()%></p>
                        <p>Description: <%=bookDto.getDescription()%></p>
                        <p>Author(s): </p>
                        <p>Publish date: <%=(bookDto.getPublishDate() == null ? "Unknown" : bookDto.getPublishDate())%></p>
                        <button>Add review</button>
                    </div>
                <% } %>
    </div>
    </div>
</body>

</html>