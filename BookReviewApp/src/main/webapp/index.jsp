<%@ page import="com.bookReviewApp.controller.util.CommandEnum"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
    <link rel="stylesheet" href="css/core.css" />
    <title>Book Review App</title>
</head>

<body>
    <div class="login">
        <center>
            <h3>Sign in!</h3>
                <div class="error">
                    <p>
                        <%
                            Object message = request.getAttribute("message");
                            if(message==null) {
                            message = "";
                            }
                            out.println(message);
                        %>
                    </p>
                </div>
            <table>
                <form action="FrontController" method="post">
                    <tr>
                        <th>Email:</th>
                        <th><input type="text" name="email" /></th>
                    </tr>
                    <th>Password:</th>
                    <th><input type="password" name="password" /></th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            <input type="submit" name="command" value=<%out.println(CommandEnum.LOGIN.getCommand());%> />
                        </th>
                    </tr>
                </form>
                <form action="FrontController" method="post">
                    <th>No account?</th>
                    <input type="hidden" name="command" value=<%out.println(CommandEnum.REDIRECT_TO_REGISTER.getCommand());%> />
                    <th><input type="submit" value="redirect To Register" /></th>
                </form>
            </table>
        </center>
    </div>
</body>

</html>