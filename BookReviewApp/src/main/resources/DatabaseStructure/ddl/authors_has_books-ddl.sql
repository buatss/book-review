CREATE TABLE `authors_has_books` (
  `authors_Author_ID` int NOT NULL,
  `books_Book_ID` int NOT NULL,
  PRIMARY KEY (`authors_Author_ID`,`books_Book_ID`),
  KEY `fk_authors_has_books_books1_idx` (`books_Book_ID`),
  KEY `fk_authors_has_books_authors1_idx` (`authors_Author_ID`),
  CONSTRAINT `fk_authors_has_books_authors1` FOREIGN KEY (`authors_Author_ID`) REFERENCES `authors` (`id`),
  CONSTRAINT `fk_authors_has_books_books1` FOREIGN KEY (`books_Book_ID`) REFERENCES `books` (`id`)
)