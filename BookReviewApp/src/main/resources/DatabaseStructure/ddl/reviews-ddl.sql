CREATE TABLE `reviews` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user` int NOT NULL,
  `book` int NOT NULL,
  `description` text,
  `grade` float(2,1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `User_idx` (`user`),
  KEY `Book_idx` (`book`),
  CONSTRAINT `book_id` FOREIGN KEY (`book`) REFERENCES `books` (`id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user`) REFERENCES `users` (`id`)
)