CREATE TABLE `books` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` text,
  `Published` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `book_info` (`title`)
)