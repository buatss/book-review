--example for users
--middlename is not mandatory, can be null

INSERT INTO users (Nickname, email, password)
VALUES ('testUser', 'test@test.test','testpassword');

--example for book
INSERT INTO books (Title, Description)
VALUES ('testTitle', 'testDescription');

--example for review
INSERT INTO reviews (User, Book, Description, Grade)
VALUES (1 , 1 ,'testDescription', 5.0);

--example for authors_has_books
insert into authors_has_books (authors_Author_ID , books_Book_ID)
values (1, 1);

--example for authors
insert into authors (firstname , lastname, middlename, birthdate)
values (firstname, lastname, middlename, '1990-01-01');