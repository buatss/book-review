package com.bookReviewApp.controller.command;

import com.bookReviewApp.controller.exception.ControllerException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface Command {
    void process(HttpServletRequest request, HttpServletResponse response) throws ControllerException, ServletException, IOException;

    static String prepareUri(HttpServletRequest request) {
        String uri = request.getRequestURI().replace("/", "");
        if (uri.length() == 0) {
            uri = "home";
        }
        return uri;
    }
}