package com.bookReviewApp.controller.command.impl;

import com.bookReviewApp.controller.command.Command;
import com.bookReviewApp.controller.exception.ControllerException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RedirectToRegister implements Command {

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) throws ControllerException, ServletException, IOException {
        response.sendRedirect("register.jsp");
    }
}
