package com.bookReviewApp.controller.command.impl;

import com.bookReviewApp.controller.command.Command;
import com.bookReviewApp.controller.exception.ControllerException;
import com.bookReviewApp.domain.User;
import com.bookReviewApp.service.exception.ServiceException;
import com.bookReviewApp.service.serviceLogic.UserService;
import com.bookReviewApp.service.serviceLogic.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginCommand implements Command {
    UserService userService = new UserServiceImpl();

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) throws ControllerException, ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        User user = new User();
        user.setNickname("");
        user.setEmail(email);
        user.setPassword(password);
        response.setContentType("text/html;charset=windows-1255");
        try {
            userService.login(user);
            response.sendRedirect("home.jsp");
        } catch (ServiceException e) {
            String message = "Wrong nickname, email or password, try again";
            request.setAttribute("message", message);
            request.getRequestDispatcher("index.jsp").forward(request,response);
            throw new ControllerException("Login process failed, redirect to index.jsp.");
        }
    }
}

