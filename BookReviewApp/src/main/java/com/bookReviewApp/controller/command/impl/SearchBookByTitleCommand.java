package com.bookReviewApp.controller.command.impl;

import com.bookReviewApp.controller.command.Command;
import com.bookReviewApp.controller.exception.ControllerException;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.dto.BookDto;
import com.bookReviewApp.service.exception.ServiceException;
import com.bookReviewApp.service.serviceLogic.BookService;
import com.bookReviewApp.service.serviceLogic.impl.BookServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.bookReviewApp.util.Util.*;

public class SearchBookByTitleCommand implements Command {
    BookService bookService = new BookServiceImpl();

    @Override
    public void process(HttpServletRequest request, HttpServletResponse response) throws ControllerException, ServletException, IOException {
        Page<BookDto> pageRequest = new Page<BookDto>();
        Page<BookDto> pageResponse;
        BookDto filter = new BookDto();
        List<BookDto> bookPage;


        String title = request.getParameter("search");
        filter.setTitle(title);

        pageRequest.setFilter(filter);
        pageRequest.setLimit(5);
        pageRequest.setTotalElements(10);
        pageRequest.setPageNumber(1);
        try {
            pageResponse = bookService.showBooks(convertBookDtoPageToBookPage(pageRequest));
            bookPage = pageResponse.getElements();
            request.setAttribute("books", bookPage);
            request.getRequestDispatcher("search.jsp").forward(request, response);
        } catch (ServiceException e) {
            throw new ControllerException("Finding page of books failed.");
        }

    }

}
