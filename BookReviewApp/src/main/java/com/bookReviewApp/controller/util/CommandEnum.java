package com.bookReviewApp.controller.util;

public enum CommandEnum
{
    LOGIN("login"),
    REGISTER("register"),
    REDIRECT_TO_CONTENT("redirectToRegister"),
    REDIRECT_TO_REGISTER("redirectToContent"),
    FIND_BOOK_BY_TITLE("searchBookByTitle");

    private final String command;

    CommandEnum(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
}