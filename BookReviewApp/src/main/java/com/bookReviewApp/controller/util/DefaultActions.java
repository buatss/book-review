package com.bookReviewApp.controller.util;

import com.bookReviewApp.domain.Book;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.dto.BookDto;
import com.bookReviewApp.service.exception.ServiceException;
import com.bookReviewApp.service.serviceLogic.BookService;
import com.bookReviewApp.service.serviceLogic.impl.BookServiceImpl;

import java.util.Collections;
import java.util.List;

public class DefaultActions {
    //shows default page of books in home.jsp before requesting search by user
    public static List<BookDto> defaultPageOfBooks() {
        BookService bookService = new BookServiceImpl();

        Book filter = new Book();
        Page<BookDto> pageResponse = new Page<>();
        List<BookDto> bookDtoPage = Collections.emptyList();

        String title = "";
        filter.setTitle(title);

        Page<Book> pageRequest = new Page<>();
        pageRequest.setLimit(5);
        pageRequest.setTotalElements(10);
        pageRequest.setPageNumber(1);
        pageRequest.setFilter(filter);
        pageRequest.setSortBy("title");

        try {
            pageResponse = bookService.showBooks(pageRequest);
            return bookDtoPage = pageResponse.getElements();
        } catch (ServiceException ignored) {
            return null;
        }
    }
}
