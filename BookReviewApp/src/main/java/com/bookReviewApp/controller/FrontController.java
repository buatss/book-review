package com.bookReviewApp.controller;

import com.bookReviewApp.controller.command.Command;
import com.bookReviewApp.controller.command.impl.*;
import com.bookReviewApp.controller.exception.ControllerException;
import com.bookReviewApp.controller.util.CommandEnum;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FrontController extends HttpServlet {
    Map<String, Command> commandMap = new HashMap<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
//        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
//        super.doPost(req, resp);
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        commandMap = new HashMap<>();
        commandMap.put(CommandEnum.LOGIN.getCommand(), new LoginCommand());
        commandMap.put(CommandEnum.REGISTER.getCommand(), new RegisterCommand());
        commandMap.put(CommandEnum.REDIRECT_TO_REGISTER.getCommand(), new RedirectToRegister());
        commandMap.put(CommandEnum.REDIRECT_TO_CONTENT.getCommand(), new RedirectToContent());
        commandMap.put(CommandEnum.FIND_BOOK_BY_TITLE.getCommand(), new SearchBookByTitleCommand());
    }

    private void doProcess(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String commandType = req.getParameter("command");
        Command command = commandMap.get(commandType);
        try {
            command.process(req, resp);
        } catch (ControllerException e) {
            e.printStackTrace();
        }
    }

}

