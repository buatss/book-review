package com.bookReviewApp.util;

import com.bookReviewApp.domain.Book;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.dto.BookDto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class Util {
    public static String convertNullStringToEmpty(String s) {
        try {
            if (isNull(s)) {
                return "";
            }
        } catch (NullPointerException e) {
            return "";
        }
        return s;
    }

    public static Book convertNullBookToEmpty(Book b) {
        if (b.getId() == null)
            b.setId(0L);
        if (b.getTitle() == null)
            b.setTitle("");
        if (b.getPublishDate() == null)
            b.setPublishDate(new Date());
        if (b.getDescription() == null)
            b.setDescription("");
        if (isNull(b)) {
            return new Book(0L, "", "", new Date());
        }
        return b;
    }

    public static BookDto convertNullBookToEmpty(BookDto b) {
        if (b.getId() == null)
            b.setId(0L);
        if (b.getTitle() == null)
            b.setTitle("");
        if (b.getPublishDate() == null)
            b.setPublishDate(new Date());
        if (b.getDescription() == null)
            b.setDescription("");
        return b;
    }

    public static List<BookDto> convertListBookToListBookDto(List<Book> bookList) {
        return bookList
                .stream()
                .map(BookDto::new)
                .collect(Collectors.toList());
    }

    public static List<Book> convertListBookDtoToListBook(List<BookDto> bookDtoList) {
        List<Book> bookList = bookDtoList
                .stream()
                .map(Book::new)
                .collect(Collectors.toList());
        return bookList;
    }

    public static Page<BookDto> convertBookPageToBookDtoPage(Page<Book> bookPage) {
        Page<BookDto> pageBookDto = new Page<>();
        pageBookDto.setPageNumber(bookPage.getPageNumber());
        pageBookDto.setTotalElements(bookPage.getTotalElements());
        pageBookDto.setLimit(bookPage.getLimit());
        pageBookDto.setElements(convertListBookToListBookDto(bookPage.getElements()));
        pageBookDto.setFilter(new BookDto(bookPage.getFilter()));
        pageBookDto.setSortBy(bookPage.getSortBy());
        pageBookDto.setDirection(bookPage.getDirection());
        return pageBookDto;
    }

    public static Page<Book> convertBookDtoPageToBookPage(Page<BookDto> bookDtoPage) {
        Page<Book> pageBook = new Page<>();
        pageBook.setPageNumber(bookDtoPage.getPageNumber());
        pageBook.setTotalElements(bookDtoPage.getTotalElements());
        pageBook.setLimit(bookDtoPage.getLimit());
        pageBook.setElements(convertListBookDtoToListBook(bookDtoPage.getElements()));
        pageBook.setFilter(new Book(bookDtoPage.getFilter()));
        pageBook.setSortBy(bookDtoPage.getSortBy());
        pageBook.setDirection(bookDtoPage.getDirection());
        return pageBook;
    }
}