package com.bookReviewApp.service.serviceLogic.impl;

import com.bookReviewApp.dao.DaoFactory;
import com.bookReviewApp.dao.exception.DaoException;
import com.bookReviewApp.dao.repository.ReviewDao;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.Review;
import com.bookReviewApp.domain.dto.ReviewDto;
import com.bookReviewApp.service.exception.ServiceException;
import com.bookReviewApp.service.serviceLogic.ReviewService;
import com.bookReviewApp.service.validator.ServiceValidator;

public class ReviewServiceImpl implements ReviewService {

    ServiceValidator validator = new ServiceValidator();
    private final ReviewDao reviewDao = DaoFactory.getInstance().getReviewDao();

    @Override
    public ReviewDto saveReview(Review review) throws ServiceException {
        validator.validate(review);
        try {
            ReviewDto reviewDto = reviewDao.saveReview(review);
            return reviewDto;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteReview(Review review) throws ServiceException {
        try {
            validator.validate(review);
            reviewDao.deleteReview(review);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Page<Review> showReviews(Page<Review> reviewPageRequest) throws ServiceException {
        try {
            validator.validateReviewPage(reviewPageRequest);
            Page<Review> page = reviewDao.findReviewByFilter(reviewPageRequest);
            return page;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

}
