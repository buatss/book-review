package com.bookReviewApp.service.serviceLogic.impl;

import com.bookReviewApp.dao.DaoFactory;
import com.bookReviewApp.dao.exception.DaoException;
import com.bookReviewApp.dao.repository.BookDao;
import com.bookReviewApp.domain.Book;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.dto.BookDto;
import com.bookReviewApp.service.exception.ServiceException;
import com.bookReviewApp.service.serviceLogic.BookService;
import com.bookReviewApp.service.validator.ServiceValidator;
import com.bookReviewApp.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.bookReviewApp.util.Util.convertListBookToListBookDto;

public class BookServiceImpl implements BookService {

    private final BookDao bookDao = DaoFactory.getInstance().getBookDao();
    private final ServiceValidator validator = new ServiceValidator();

    @Override
    public BookDto saveBook(Book book) throws ServiceException {
        try {
            validator.validate(book);
            return bookDao.saveBook(book);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteBook(Book book) throws ServiceException {
        try {
            validator.validate(book);
            bookDao.deleteBook(book);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }

    }

    @Override
    public Page<BookDto> showBooks(Page<Book> bookPageRequest) throws ServiceException {
        try {
            validator.validateBookPage(bookPageRequest);
            return Util.convertBookPageToBookDtoPage(bookDao.findBookByFilter(bookPageRequest));
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

}
