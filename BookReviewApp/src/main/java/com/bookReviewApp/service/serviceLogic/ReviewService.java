package com.bookReviewApp.service.serviceLogic;

import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.Review;
import com.bookReviewApp.domain.dto.ReviewDto;
import com.bookReviewApp.service.exception.ServiceException;

public interface ReviewService {
    ReviewDto saveReview(Review review) throws ServiceException;

    void deleteReview(Review review) throws ServiceException;

    Page<Review> showReviews(Page<Review> bookPageRequest) throws ServiceException;
}
