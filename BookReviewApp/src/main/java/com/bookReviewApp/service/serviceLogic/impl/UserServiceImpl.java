package com.bookReviewApp.service.serviceLogic.impl;

import com.bookReviewApp.dao.DaoFactory;
import com.bookReviewApp.dao.exception.DaoException;
import com.bookReviewApp.dao.repository.UserDao;
import com.bookReviewApp.domain.User;
import com.bookReviewApp.domain.dto.UserDto;
import com.bookReviewApp.service.exception.ServiceException;
import com.bookReviewApp.service.serviceLogic.UserService;
import com.bookReviewApp.service.validator.ServiceValidator;

public class UserServiceImpl implements UserService {

    private final UserDao userDao = DaoFactory.getInstance().getUserDao();
    private final ServiceValidator validator = new ServiceValidator();

    @Override
    public UserDto registerUser(User user) throws ServiceException {
        try {
            validator.validate(user);
            UserDto userDto = userDao.saveUser(user);
            return userDto;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public UserDto login(User user) throws ServiceException {
        try {
            validator.validate(user);
            UserDto userDto = userDao.findUserByEmailAndPassword(user);
            return userDto;
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }
}
