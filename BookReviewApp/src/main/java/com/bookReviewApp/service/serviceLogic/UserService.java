package com.bookReviewApp.service.serviceLogic;

import com.bookReviewApp.domain.User;
import com.bookReviewApp.domain.dto.UserDto;
import com.bookReviewApp.service.exception.ServiceException;

public interface UserService {
    UserDto registerUser(User user) throws ServiceException;

    UserDto login(User user) throws ServiceException;
}
