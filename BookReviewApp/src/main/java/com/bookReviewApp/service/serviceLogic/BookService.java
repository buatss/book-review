package com.bookReviewApp.service.serviceLogic;

import com.bookReviewApp.domain.Book;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.dto.BookDto;
import com.bookReviewApp.service.exception.ServiceException;

public interface BookService {
    BookDto saveBook(Book book) throws ServiceException;

    void deleteBook(Book book) throws ServiceException;

    Page<BookDto> showBooks(Page<Book> bookPageRequest) throws ServiceException;
}
