package com.bookReviewApp.service.validator;

import com.bookReviewApp.domain.Book;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.Review;
import com.bookReviewApp.domain.User;
import com.bookReviewApp.service.exception.ServiceException;

import java.util.Date;

import static java.util.Objects.isNull;

public class ServiceValidator {
    public void validate(User user) throws ServiceException {
        isValidObject(user, " user is null ");
        isValidString(user.getEmail(), "email");
        isValidString(user.getNickname(), "nickname");
        isValidString(user.getPassword(), "password");
    }

    public void validate(Book book) throws ServiceException {
        isValidObject(book, " book is null ");
        isValidString(book.getTitle(), "title");
        isValidString(book.getDescription(), "description");
        isValidDate(book.getPublishDate(), "publish date");
    }

    public void validateBookPage(Page<Book> pageRequest) throws ServiceException {
        if(isNull(pageRequest)){
            throw new ServiceException("BookPageRequest is null or empty");
        }
    }

    public void validateReviewPage(Page<Review> pageRequest) throws ServiceException {
        if(isNull(pageRequest)){
            throw new ServiceException("BookPageRequest is null or empty");
        }
    }

    public void validate(Review review) throws ServiceException {
        isValidObject(review, " review is null ");
        isValidObject(review.getBookID(), " book ");
        isValidObject(review.getUserID(), " user ");
        isValidString(review.getDescription(), " description ");
        isValidGrade(review.getGrade(),"grade");
    }

    private void isValidObject(Object object, String msg) throws ServiceException {
        if (isNull(object)) {
            throw new ServiceException(msg);
        }
    }

    private void isValidString(final String string, final String subject) throws ServiceException {
        if (isNull(string)) {
            throw new ServiceException(subject + " is null or empty ");
        }
    }

    private void isValidDate(final Date date, final String subject) throws ServiceException {
        if(isNull(date)){
            throw new ServiceException(subject + " is null or empty");
        }
    }

    private void isValidGrade(final Float grade, final String subject) throws  ServiceException{
        if(isNull(grade)){
            throw new ServiceException(subject + " is null or empty");
        }
    }


}
