package com.bookReviewApp.service;

import com.bookReviewApp.dao.DaoFactory;
import com.bookReviewApp.service.serviceLogic.BookService;
import com.bookReviewApp.service.serviceLogic.ReviewService;
import com.bookReviewApp.service.serviceLogic.UserService;
import com.bookReviewApp.service.serviceLogic.impl.BookServiceImpl;
import com.bookReviewApp.service.serviceLogic.impl.ReviewServiceImpl;
import com.bookReviewApp.service.serviceLogic.impl.UserServiceImpl;

public class ServiceFactory {
    private static final ServiceFactory INSTANCE = new ServiceFactory(DaoFactory.getInstance());

    private final UserService userService;
    private final BookService bookService;
    private final ReviewService reviewService;

    public ServiceFactory(DaoFactory instance) {
        userService = new UserServiceImpl();
        bookService = new BookServiceImpl();
        reviewService = new ReviewServiceImpl();
    }

    public UserService getUserService() {
        return userService;
    }

    public BookService getBookService() {
        return bookService;
    }

    public ReviewService getReviewService() {
        return reviewService;
    }
}
