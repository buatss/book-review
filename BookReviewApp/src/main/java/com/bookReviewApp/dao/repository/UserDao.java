package com.bookReviewApp.dao.repository;

import com.bookReviewApp.dao.exception.DaoException;
import com.bookReviewApp.domain.User;
import com.bookReviewApp.domain.dto.UserDto;

public interface UserDao{

    UserDto saveUser(User user) throws DaoException;

    UserDto findUserByEmailAndPassword(User user) throws DaoException;

    void deleteUser(User user) throws DaoException;

}
