package com.bookReviewApp.dao.repository.impl;

import com.bookReviewApp.dao.connection.ConnectionPool;
import com.bookReviewApp.dao.exception.DaoException;
import com.bookReviewApp.dao.repository.UserDao;
import com.bookReviewApp.domain.User;
import com.bookReviewApp.domain.dto.UserDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Objects.isNull;

public class UserDaoImpl extends AbstractDao implements UserDao {

    private static final String FIND_USER_BY_EMAIL_OR_NICKNAME_QUERY = "SELECT u.Nickname, u.email FROM users u WHERE u.email=? OR u.Nickname=?";
    private static final String SAVE_USER_QUERY = "INSERT INTO users (Nickname, email, password) VALUES (?, ?, ?)";
    private static final String FIND_USER_BY_EMAIL_AND_PASSWORD_QUERY = "SELECT u.id, u.email, u.Nickname, u.password, u.registerDate FROM users u WHERE u.email=? AND u.password=?";
    private static final String DELETE_USER_BY_EMAIL_QUERY = "DELETE FROM users u WHERE u.email=?";

    public UserDaoImpl(final ConnectionPool connectionPool) {
        super(connectionPool);
    }

    @Override
    public UserDto saveUser(User user) throws DaoException {
        final List<Object> parameters1 = Arrays.asList(user.getEmail(), user.getNickname());
        final List<Object> parameters2 = Arrays.asList(
                user.getNickname(),
                user.getEmail(),
                user.getPassword()
        );

        Connection connection = getConnection(false);
        ResultSet resultSet = null;
        int affectedRows = 0;
        try {
            PreparedStatement preparedStatement1 = getPreparedStatement(FIND_USER_BY_EMAIL_OR_NICKNAME_QUERY, connection, parameters1);
            PreparedStatement preparedStatement2 = getPreparedStatement(SAVE_USER_QUERY, connection, parameters2);

            resultSet = preparedStatement1.executeQuery();
            if (!resultSet.next()) {
                affectedRows = preparedStatement2.executeUpdate();
            }
            connection.commit();

            processAbnormalCase(affectedRows < 1, "User is not registered, such account exists.");
            return new UserDto(user);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(resultSet);
            release(connection);
        }
        return null;
    }

    @Override
    public UserDto findUserByEmailAndPassword(User user) throws DaoException {
        List<Object> parameters = Arrays.asList(
                user.getEmail(),
                user.getPassword()
        );
        Connection connection = getConnection(true);

        try {
            PreparedStatement preparedStatement = getPreparedStatement(FIND_USER_BY_EMAIL_AND_PASSWORD_QUERY, connection, parameters);
            ResultSet resultSet = preparedStatement.executeQuery();

            UserDto userDto = null;

            while (resultSet.next()){
                final long id = resultSet.getLong(1);
                final String nickname = resultSet.getString(2);
                final String email = resultSet.getString(3);
                userDto = new UserDto(id, nickname, email);
            }
            processAbnormalCase(isNull(userDto),"No user found");
            return userDto;
        } catch (SQLException | DaoException e) {
            e.printStackTrace();
            throw new DaoException(e);
        } finally {
            release(connection);
        }
    }

    @Override
    public void deleteUser(User user) throws DaoException {
        final List<Object> parameterID = Collections.singletonList(
          user.getEmail()
        );

        Connection connection = getConnection(true);
        try {
            PreparedStatement preparedStatement = getPreparedStatement(DELETE_USER_BY_EMAIL_QUERY, connection, parameterID);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException(e);
        } finally {
            release(connection);
        }
    }

}
