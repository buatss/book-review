package com.bookReviewApp.dao.repository.impl;

import com.bookReviewApp.dao.connection.ConnectionPool;
import com.bookReviewApp.dao.exception.DaoException;
import com.bookReviewApp.dao.repository.BookDao;
import com.bookReviewApp.domain.Book;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.dto.BookDto;
import com.bookReviewApp.util.Util;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.bookReviewApp.util.Util.convertNullBookToEmpty;
import static java.util.Objects.isNull;

public class BookDaoImpl extends AbstractDao implements BookDao {

    private static final String FIND_BOOK_BY_TITLE_QUERY = "SELECT b.id, b.title, b.description, b.published from books b WHERE b.title=?";
    private static final String SAVE_BOOK_QUERY = "INSERT INTO books (title, description, published) VALUES (?,?,?)";
    private static final String DELETE_BOOK_BY_TITLE_AND_AUTHOR_QUERY = "DELETE FROM books b WHERE b.title=?";
    private static final String FIND_PAGE_FILTERED_SORTED =
            "SELECT * FROM books b " +
                    "WHERE UPPER(b.title) " +
                    "LIKE CONCAT(UPPER(?), '%%') " +
                    "ORDER BY b.%s LIMIT ? OFFSET ?;";
    private static final String COUNT_ALL_FILTERED_SORTED =
            "SELECT count(b.id) FROM books b " +
                    "WHERE UPPER(b.title) LIKE CONCAT('%', UPPER(?)) ";
    private static final String FIND_PAGE_SORTED_BY_TITLE = "SELECT * FROM books b " +
            "ORDER BY b.title LIMIT ? OFFSET ?;";

    public BookDaoImpl(ConnectionPool connectionPool) {
        super(connectionPool);
    }

    @Override
    public BookDto saveBook(Book book) throws DaoException {
        List<Object> parameters1 = Collections.singletonList(
                book.getTitle()
        );
        final List<Object> parameters2 = Arrays.asList(
                book.getTitle(),
                book.getDescription(),
                book.getPublishDate()
        );

        Connection connection = getConnection(false);
        ResultSet resultSet = null;
        int affectedRows = 0;

        try {
            PreparedStatement preparedStatement1 = getPreparedStatement(FIND_BOOK_BY_TITLE_QUERY, connection, parameters1);
            PreparedStatement preparedStatement2 = getPreparedStatement(SAVE_BOOK_QUERY, connection, parameters2);
            resultSet = preparedStatement1.executeQuery();

            if (!resultSet.next()) {
                affectedRows = preparedStatement2.executeUpdate();
            }
            connection.commit();

            processAbnormalCase(affectedRows < 1, "Book not saved, such book already exists.");

            return new BookDto(book);
        } catch (SQLException | DaoException e) {
            e.printStackTrace();
            throw new DaoException(e);
        } finally {
            close(resultSet);
            release(connection);
        }
    }

    @Override
    public BookDto findBookByTitleAndAuthor(Book book) throws DaoException {
        List<Object> parameters = Collections.singletonList(
                book.getTitle()
        );
        Connection connection = getConnection(true);

        ResultSet resultSet = null;
        try {
            PreparedStatement preparedStatement = getPreparedStatement(FIND_BOOK_BY_TITLE_QUERY, connection, parameters);
            resultSet = preparedStatement.executeQuery();

            BookDto bookDto = null;

            while (resultSet.next()) {
                final Long id = resultSet.getLong(1);
                final String title = resultSet.getString(2);
                final String description = resultSet.getString(3);
                final Date published = resultSet.getDate(4);
                bookDto = new BookDto(id, title, description, published);
            }
            processAbnormalCase(isNull(bookDto), "No book found");
            return bookDto;
        } catch (SQLException | DaoException e) {
            e.printStackTrace();
            throw new DaoException(e);
        } finally {
            close(resultSet);
            release(connection);
        }
    }

    @Override
    public void deleteBook(Book book) throws DaoException {
        final List<Object> parameters = Collections.singletonList(
                book.getId()
        );

        Connection connection = getConnection(true);
        try {
            PreparedStatement preparedStatement = getPreparedStatement(DELETE_BOOK_BY_TITLE_AND_AUTHOR_QUERY, connection, parameters);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException(e);
        } finally {
            release(connection);
        }
    }

    @Override
    public Page<Book> findBookByFilter(Page<Book> bookPage) throws DaoException {
        final List<Object> parameters1 = Collections.singletonList(
                convertNullBookToEmpty(bookPage.getFilter()).getTitle().toUpperCase()
        );
        final List<Object> parameters2 = Arrays.asList(
                convertNullBookToEmpty(bookPage.getFilter()).getTitle().toUpperCase(),
                bookPage.getLimit(),
                prepareOffset(bookPage.getPageNumber(), bookPage.getLimit())
        );

        final String getPageQuery = setSortAndDirection(FIND_PAGE_FILTERED_SORTED, bookPage.getSortBy(), bookPage.getDirection());
        final Connection connection = getConnection(false);

        try (final PreparedStatement preparedStatementCountAllProducts = getPreparedStatement(COUNT_ALL_FILTERED_SORTED, connection, parameters1);
             final PreparedStatement preparedStatementGetBooksPageList = getPreparedStatement(getPageQuery, connection, parameters2);
             final ResultSet totalProducts = preparedStatementCountAllProducts.executeQuery();
             final ResultSet productsPageList = preparedStatementGetBooksPageList.executeQuery()) {

            connection.commit();

            return getBookPageable(bookPage, totalProducts, productsPageList);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            release(connection);
        }
    }

    private Page<Book> getBookPageable(Page<Book> daoBookPageable,
                                       ResultSet totalProducts,
                                       ResultSet productsPageList) throws SQLException {
        final Page<Book> page = new Page<>();
        long totalElements = 0L;
        while (totalProducts.next()) {
            totalElements = totalProducts.getLong(1);
        }
        final List<Book> rows = new ArrayList<>();
        while (productsPageList.next()) {
            long id = productsPageList.getLong(1);
            String title = productsPageList.getString(2);
            String description = productsPageList.getString(3);
            Date published = productsPageList.getDate(4);
            rows.add(new Book(id, title, description,published));
        }
        page.setPageNumber(daoBookPageable.getPageNumber());
        page.setLimit(daoBookPageable.getLimit());
        page.setTotalElements(totalElements);
        page.setElements(rows);
        page.setFilter(Util.convertNullBookToEmpty(daoBookPageable.getFilter()));
//        page.setFilter(daoBookPageable.getFilter());
        page.setSortBy(daoBookPageable.getSortBy());
        page.setDirection(daoBookPageable.getDirection());
        return page;
    }
}
