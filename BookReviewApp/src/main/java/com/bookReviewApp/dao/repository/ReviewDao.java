package com.bookReviewApp.dao.repository;

import com.bookReviewApp.dao.exception.DaoException;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.Review;
import com.bookReviewApp.domain.dto.ReviewDto;

public interface ReviewDao{
    ReviewDto saveReview(Review review) throws DaoException;

    ReviewDto findReviewByID(Review review) throws DaoException;

    void deleteReview(Review review) throws DaoException;

    Page<Review> findReviewByFilter(Page<Review> bookPageable) throws DaoException;
}
