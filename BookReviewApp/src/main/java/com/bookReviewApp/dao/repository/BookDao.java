package com.bookReviewApp.dao.repository;

import com.bookReviewApp.dao.exception.DaoException;
import com.bookReviewApp.domain.Book;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.dto.BookDto;

public interface BookDao{
    BookDto saveBook(Book book) throws DaoException;

    BookDto findBookByTitleAndAuthor(Book book) throws DaoException;

    void deleteBook(Book book) throws DaoException;

    Page<Book> findBookByFilter(Page<Book> bookPageable) throws DaoException;
}
