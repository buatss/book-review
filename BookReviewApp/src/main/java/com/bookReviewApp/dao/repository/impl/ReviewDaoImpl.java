package com.bookReviewApp.dao.repository.impl;


import com.bookReviewApp.dao.connection.ConnectionPool;
import com.bookReviewApp.dao.exception.DaoException;
import com.bookReviewApp.dao.repository.ReviewDao;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.domain.Review;
import com.bookReviewApp.domain.dto.ReviewDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.bookReviewApp.util.Util.convertNullStringToEmpty;
import static java.util.Objects.isNull;

public class ReviewDaoImpl extends AbstractDao implements ReviewDao {
    public ReviewDaoImpl(ConnectionPool connectionPool) {
        super(connectionPool);
    }

    private static final String FIND_REVIEW_BY_ID_QUERY = "SELECT r.Review_ID, r.User, R.Book, r.Description, R.Grade FROM reviews r WHERE r.Review_ID=?";
    private static final String SAVE_REVIEW_QUERY = "INSERT INTO reviews (User, Book, Description, Grade) VALUES (?,?,?,?)";
    private static final String DELETE_REVIEW_BY_ID_QUERY = "DELETE FROM reviews r WHERE r.Review_ID=?";
    private static final String FIND_PAGE_FILTERED_SORTED =
            "SELECT * FROM reviews r " +
                    "WHERE UPPER(r.Description) LIKE CONCAT('%%', UPPER(?), '%%') " +
                    "ORDER BY r.%s %s LIMIT ? OFFSET ?;";
    private static final String COUNT_ALL_FILTERED_SORTED =
            "SELECT count(r.Review_ID) FROM reviews r " +
                    "WHERE UPPER(r.Description) LIKE CONCAT('%', UPPER(?), '%') ";

    @Override
    public ReviewDto saveReview(Review review) throws DaoException {
        List<Object> parameters1 = Collections.singletonList(
                review.getId()
        );
        final List<Object> parameters2 = Arrays.asList(
                review.getUserID(),
                review.getBookID(),
                review.getDescription(),
                review.getGrade()
        );

        Connection connection = getConnection(false);
        ResultSet resultSet = null;
        int affectedRows = 0;

        try {
            PreparedStatement preparedStatement1 = getPreparedStatement(FIND_REVIEW_BY_ID_QUERY, connection, parameters1);
            PreparedStatement preparedStatement2 = getPreparedStatement(SAVE_REVIEW_QUERY, connection, parameters2);
            resultSet = preparedStatement1.executeQuery();

            if (!resultSet.next()) {
                try {
                    affectedRows = preparedStatement2.executeUpdate();
                } catch (SQLIntegrityConstraintViolationException e) {
                    throw new DaoException("Invalid user or book in review.");
                }
            }
            connection.commit();

            processAbnormalCase(affectedRows < 1, "Review not saved, such book already exists.");

            return new ReviewDto(review);
        } catch (SQLException | DaoException e) {
            e.printStackTrace();
            throw new DaoException(e);
        } finally {
            close(resultSet);
            release(connection);
        }
    }

    @Override
    public ReviewDto findReviewByID(Review review) throws DaoException {
        List<Object> parameter = Collections.singletonList(
                review.getId()
        );

        Connection connection = getConnection(true);
        ResultSet resultSet = null;

        ReviewDto reviewDto = null;

        try {
            PreparedStatement preparedStatement = getPreparedStatement(FIND_REVIEW_BY_ID_QUERY, connection, parameter);

            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                final Long id = resultSet.getLong(1);
                final Long bookID = resultSet.getLong(2);
                final Long userID = resultSet.getLong(3);
                final String description = resultSet.getString(4);
                final Float grade = resultSet.getFloat(5);
                reviewDto = new ReviewDto(id, bookID, userID, description, grade);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(resultSet);
            release(connection);
        }

        processAbnormalCase(isNull(reviewDto), "No review found.");
        return reviewDto;
    }

    @Override
    public void deleteReview(Review review) throws DaoException {
        List<Object> parameter = Collections.singletonList(
                review.getId()
        );

        Connection connection = getConnection(true);

        try {
            PreparedStatement preparedStatement = getPreparedStatement(DELETE_REVIEW_BY_ID_QUERY, connection, parameter);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DaoException(e);
        } finally {
            release(connection);
        }
    }

    @Override
    public Page<Review> findReviewByFilter(Page<Review> reviewPageable) throws DaoException {
        final List<Object> parameters1 = Arrays.asList(
                convertNullStringToEmpty(reviewPageable.getFilter().getDescription()).toUpperCase(),
                convertNullStringToEmpty(String.valueOf(reviewPageable.getFilter().getGrade())).toUpperCase()
        );
        final List<Object> parameters2 = Arrays.asList(
                convertNullStringToEmpty(reviewPageable.getFilter().getDescription()).toUpperCase(),
                convertNullStringToEmpty(String.valueOf(reviewPageable.getFilter().getGrade())).toUpperCase(),
                reviewPageable.getLimit(),
                prepareOffset(reviewPageable.getPageNumber(), reviewPageable.getLimit())
        );
        final String getPageQuery = setSortAndDirection(FIND_PAGE_FILTERED_SORTED, reviewPageable.getSortBy(), reviewPageable.getDirection());
        final Connection connection = getConnection(false);
        try (final PreparedStatement preparedStatementCountAllProducts = getPreparedStatement(COUNT_ALL_FILTERED_SORTED, connection, parameters1);
             final PreparedStatement preparedStatementGetReviewsPageList = getPreparedStatement(getPageQuery, connection, parameters2);
             final ResultSet totalProducts = preparedStatementCountAllProducts.executeQuery();
             final ResultSet productsPageList = preparedStatementGetReviewsPageList.executeQuery()) {
            connection.commit();
            return getReviewPageable(reviewPageable, totalProducts, productsPageList);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            release(connection);
        }
    }

    private Page<Review> getReviewPageable(Page<Review> daoProductPageable,
                                           ResultSet totalProducts,
                                           ResultSet productsPageList) throws SQLException {
        final Page<Review> pageable = new Page<>();
        long totalElements = 0L;
        while (totalProducts.next()) {
            totalElements = totalProducts.getLong(1);
        }
        final List<Review> rows = new ArrayList<>();
        while (productsPageList.next()) {
            long id = productsPageList.getLong(1);
            long book_ID = productsPageList.getLong(2);
            long user_ID = productsPageList.getLong(3);
            String description = productsPageList.getString(4);
            float grade = productsPageList.getFloat(5);
            rows.add(new Review(id, book_ID, user_ID, description, grade));
        }
        pageable.setPageNumber(daoProductPageable.getPageNumber());
        pageable.setLimit(daoProductPageable.getLimit());
        pageable.setTotalElements(totalElements);
        pageable.setElements(rows);
        pageable.setFilter(daoProductPageable.getFilter());
        pageable.setSortBy(daoProductPageable.getSortBy());
        pageable.setDirection(daoProductPageable.getDirection());
        return pageable;
    }
}


