package com.bookReviewApp.dao.exception;

public class DaoException extends Exception{
    public DaoException(Throwable exception) {
        super(exception);
    }

    public DaoException(String message){
        super(message);
    }
}
