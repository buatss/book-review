package com.bookReviewApp.dao;

import com.bookReviewApp.dao.config.DatabaseConfig;
import com.bookReviewApp.dao.connection.ConnectionPool;
import com.bookReviewApp.dao.connection.impl.ConnectionPoolImpl;
import com.bookReviewApp.dao.repository.UserDao;
import com.bookReviewApp.dao.repository.BookDao;
import com.bookReviewApp.dao.repository.ReviewDao;
import com.bookReviewApp.dao.repository.impl.BookDaoImpl;
import com.bookReviewApp.dao.repository.impl.ReviewDaoImpl;
import com.bookReviewApp.dao.repository.impl.UserDaoImpl;

public class DaoFactory {
    private static final DaoFactory INSTANCE = new DaoFactory();
    private final ConnectionPool connectionPool = new ConnectionPoolImpl(new DatabaseConfig());
    private final UserDao userDao = new UserDaoImpl(connectionPool);
    private static final BookDao bookDao = new BookDaoImpl(INSTANCE.connectionPool);
    private static final ReviewDao reviewDao = new ReviewDaoImpl(INSTANCE.connectionPool);

    private DaoFactory() {}

    public static DaoFactory getInstance() {
        return INSTANCE;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public BookDao getBookDao() {
        return bookDao;
    }

    public ReviewDao getReviewDao(){
        return reviewDao;
    }

}
