package com.bookReviewApp.dao.connection.impl;

import com.bookReviewApp.dao.config.DatabaseConfig;
import com.bookReviewApp.dao.connection.ConnectionPool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectionPoolImpl implements ConnectionPool {

    private final DatabaseConfig dataBaseConfig;
    private final List<Connection> availableConnections = new ArrayList<>();
    private final List<Connection> usedConnections = new ArrayList<>();

    private static final int MAX_CONNECTIONS = 4;

    private void initConnectionPool() {
        for (int count = 0; count < MAX_CONNECTIONS; count++) {
            try {
                availableConnections.add(dataBaseConfig.getConnection());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ConnectionPoolImpl(DatabaseConfig dataBaseConfig){
        this.dataBaseConfig = dataBaseConfig;
        initConnectionPool();
    }

    public Connection getConnection() {
        if (availableConnections.size() == 0) {
            return null;
        } else {
            Connection con = availableConnections.remove(availableConnections.size() - 1);
            usedConnections.add(con);
            return con;
        }
    }

    public boolean releaseConnection(Connection con) {
        if (null != con) {
            usedConnections.remove(con);
            availableConnections.add(con);
            return true;
        }
        return false;
    }

    public int getFreeConnectionCount() {
        return availableConnections.size();
    }
}
