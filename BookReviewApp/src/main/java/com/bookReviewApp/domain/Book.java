package com.bookReviewApp.domain;

import com.bookReviewApp.domain.dto.BookDto;

import java.util.Date;
import java.util.Objects;

public class Book {
    private Long id;
    private String title;
    private String description;
    private Date publishDate;

    public Book() {
    }

    public Book(Long id, String title, String description, Date publishDate) {
        this.id = id;
        this.description = description;
        this.publishDate = publishDate;
        this.title = title;
    }

    public Book(BookDto bookDto) {
        this.id = bookDto.getId();
        this.description = bookDto.getDescription();
        this.title = bookDto.getTitle();
        this.publishDate = bookDto.getPublishDate();
    }


    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public String getTitle() {
        return title;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", publishDate=" + publishDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id.equals(book.id) && title.equals(book.title) && description.equals(book.description) && Objects.equals(publishDate, book.publishDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, description, publishDate);
    }
}