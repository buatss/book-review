package com.bookReviewApp.domain.dto;

import com.bookReviewApp.domain.Book;

import java.util.Date;
import java.util.Objects;

public class BookDto {
    private Long id;
    private String title;
    private String description;
    private Date publishDate;

    public BookDto(Long id, String title, String description, Date publishDate) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.publishDate = publishDate;
    }

    public BookDto(Book book) {
        this.id = book.getId();
        this.title = book.getTitle();
        this.description = book.getDescription();
        this.publishDate = book.getPublishDate();
    }

    public BookDto() {}

    public BookDto(BookDto bookDto) {
        this.id = bookDto.getId();
        this.description = bookDto.getDescription();
        this.publishDate = bookDto.getPublishDate();
        this.title = bookDto.getTitle();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookDto bookDto = (BookDto) o;
        return id.equals(bookDto.id) &&  description.equals(bookDto.description) && publishDate.equals(bookDto.publishDate) && title.equals(bookDto.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, publishDate, title);
    }

    @Override
    public String toString() {
        return "BookDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", publishDate=" + publishDate +
                '}';
    }
}
