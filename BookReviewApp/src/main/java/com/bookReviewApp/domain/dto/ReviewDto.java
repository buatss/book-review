package com.bookReviewApp.domain.dto;

import com.bookReviewApp.domain.Review;

import java.util.Objects;

public class ReviewDto {

    Long id;
    Long bookID;
    Long userID;
    String description;
    Float grade;

    public ReviewDto(Review review) {
        this.id = review.getId();
        this.bookID = review.getBookID();
        this.userID = review.getUserID();
        this.description = review.getDescription();
        this.grade = review.getGrade();
    }

    public ReviewDto(Long id, Long bookID, Long userID, String description, Float grade) {
        this.id = id;
        this.bookID = bookID;
        this.userID = userID;
        this.description = description;
        this.grade = grade;
    }

    public Long getId() {
        return id;
    }

    public Long getBookID() {
        return bookID;
    }

    public Long getUserID() {
        return userID;
    }

    public String getDescription() {
        return description;
    }

    public Float getGrade() {
        return grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReviewDto reviewDto = (ReviewDto) o;
        return id.equals(reviewDto.id) && bookID.equals(reviewDto.bookID) && userID.equals(reviewDto.userID) && Objects.equals(description, reviewDto.description) && Objects.equals(grade, reviewDto.grade);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bookID, userID, description, grade);
    }

    @Override
    public String toString() {
        return "ReviewDto{" +
                "id=" + id +
                ", bookID=" + bookID +
                ", userID=" + userID +
                ", description='" + description + '\'' +
                ", grade=" + grade +
                '}';
    }
}
