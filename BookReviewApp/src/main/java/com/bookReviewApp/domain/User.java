package com.bookReviewApp.domain;

import java.time.LocalDate;
import java.util.Objects;

public class User {

    Long id;
    String nickname;
    String email;
    String password;
    LocalDate registerDate;

    public User() {}

    public User(Long id, String nickname, String email, String password, LocalDate registerDate) {
        this.id = id;
        this.nickname = nickname;
        this.email = email;
        this.password = password;
        this.registerDate = registerDate;
    }

    public Long getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public LocalDate getRegisterDate() {
        return registerDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRegisterDate(LocalDate registerDate) {
        this.registerDate = registerDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id) && nickname.equals(user.nickname) && email.equals(user.email) && password.equals(user.password) && registerDate.equals(user.registerDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nickname, email, password, registerDate);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", registerDate=" + registerDate +
                '}';
    }
}

