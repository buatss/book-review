package com.bookReviewApp.domain;

public class Review {

    Long id;
    Long bookID;
    Long userID;
    String description;
    Float grade;

    public Review() {}

    public Review(Long id, Long bookID, Long userID, String description, float grade) {
        this.id = id;
        this.bookID = bookID;
        this.userID = userID;
        this.description = description;
        this.grade = grade;
    }

    public Long getId() {
        return id;
    }

    public Long getBookID() {
        return bookID;
    }

    public Long getUserID() {
        return userID;
    }

    public String getDescription() {
        return description;
    }

    public float getGrade() {
        return grade;
    }

}
