import com.bookReviewApp.controller.exception.ControllerException;
import com.bookReviewApp.domain.Book;
import com.bookReviewApp.domain.Page;
import com.bookReviewApp.service.exception.ServiceException;
import com.bookReviewApp.service.serviceLogic.BookService;
import com.bookReviewApp.service.serviceLogic.impl.BookServiceImpl;

public class Main {
    public static void main(String[] args) {
        BookService bookService = new BookServiceImpl();
        Page<Book> pageRequest = new Page<Book>();
        pageRequest.setLimit(5);
        pageRequest.setTotalElements(10);
        pageRequest.setPageNumber(1);

        Book book = new Book();
        String title = "te";
        book.setTitle(title);
        pageRequest.setFilter(book);

//        try {
//           pageRequest = bookService.showBooks(pageRequest);
//        } catch (ServiceException ignored) {
//        }
//        System.out.println(pageRequest);
//        pageRequest.getElements()
//                .stream()
//                .map(Book::getTitle)
//                .forEach(System.out::println);
//    }
    }
}
